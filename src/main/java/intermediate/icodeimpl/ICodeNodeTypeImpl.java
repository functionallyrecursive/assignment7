package intermediate.icodeimpl;

import intermediate.ICodeNodeType;

/**
 * <h1>ICodeNodeType</h1>
 *
 * <p>Node types of the intermediate code parse tree.</p>
 *
 * <p>Copyright (c) 2009 by Ronald Mak</p>
 * <p>For instructional purposes only.  No warranties.</p>
 */
public enum ICodeNodeTypeImpl implements ICodeNodeType
{
    // Program structure
    BLOCK,

    // Statements
    COMPOUND, ASSIGN, WHILE, FOR, DO_WHILE, FOR_INIT, FOR_ITR, TEST, CALL, 
    PARAMETERS, RETURN, BREAK, CONTINUE, IF, IFELSE, ELSE, CONDITION, 
    SELECT_CONSTANTS, NO_OP, IN, VAR_DECLARE, EXPRESSION,

    // Relational operators
    EQ, NE, LT, LE, GT, GE, NOT,

    // Additive operators
    ADD, SUBTRACT, OR, NEGATE,

    // Multiplicative operators
    MULTIPLY, DIVIDE, MOD, AND,

    // Operands
    VARIABLE,
    INTEGER_CONSTANT, REAL_CONSTANT,
    STRING_CONSTANT, BOOLEAN_CONSTANT,
    CHAR_CONSTANT,

    // WRITE parameter
    WRITE_PARM, 
}
