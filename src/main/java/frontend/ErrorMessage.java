package frontend;

public class ErrorMessage implements Comparable<ErrorMessage> {
	private int lineNumber;
	private String errorMessage;
	
	public ErrorMessage(int lineNumber, String errorMessage){
		this.setLineNumber(lineNumber);
		this.setErrorMessage(errorMessage);
	}

	public int getLineNumber() {
		return lineNumber;
	}

	public void setLineNumber(int lineNumber) {
		this.lineNumber = lineNumber;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	
	public int compareTo(ErrorMessage e){
		return this.getLineNumber() - e.getLineNumber();
	}
	
	public String toString(){
		return String.format("Line #%d: %s", lineNumber, errorMessage);
	}
}
