package frontend;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Simple class to handle error messages
 * @author ejost
 *
 */
public class ErrorMessageHandler {
	private ArrayList<ErrorMessage> ERROR_MESSAGES;
	
	/*
	 * Creates new handler
	 */
	public ErrorMessageHandler(){
		ERROR_MESSAGES = new ArrayList<>();
	}
	
	/**
	 * Adds a new error message
	 * @param e
	 */
	public void addErrorMessage(ErrorMessage e){
		ERROR_MESSAGES.add(e);
	}
	/**
	 * Prints error messages in sorted order
	 * @return true if at least one error message exists to stop compliation
	 */
	public boolean printErrorMessages(){
		
		Collections.sort(ERROR_MESSAGES);
		
		for (ErrorMessage e : ERROR_MESSAGES){
			System.err.println(e);
		}
		
		return !ERROR_MESSAGES.isEmpty();
	}
}
